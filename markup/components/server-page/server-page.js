
$(document).ready(function () {
    if ($('.js-datapicker').length > 0) {
        var date = new Date();
        date.setDate(date.getDate());

        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;

        $('.js-datapicker').val(today);

        $('.js-datapicker').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            orientation: "bottom right",
            todayHighlight: true,
            weekStart: 1,
            startDate: date,
        });

        $('.js-datapicker').datepicker().on("changeDate", function (e) {
            console.log($('.js-datapicker').datepicker('getFormattedDate'));
        });
    }
});
$('.js-timepicker').popover({
    trigger: 'click',
    title: 'timepicer',
    placement: 'bottom',
    template: '<div class="tooltip" role="tooltip">\n' +
        '                                <div class="arrow"></div>\n' +
        '                                <div class="tooltip-inner">\n' +
        '                                    <div class="tooltip__time" id="tooltip__time">\n' +
        '                                        <div class="tooltip__time--header"></div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="01:00">01:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="02:00">02:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="03:00">03:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="04:00">04:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="05:00">05:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="06:00">06:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="07:00">07:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="08:00">08:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="09:00">09:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="10:00">10:00</div>\n' +
        '                                        <div class="tooltip__item active" data-time="11:00">11:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="12:00">12:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="13:00">13:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="14:00">14:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="15:00">15:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="16:00">16:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="17:00">17:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="18:00">18:00</div>\n' +
        '                                        <div class="tooltip__item disabled" data-time="19:00">19:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="20:00">20:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="21:00">21:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="22:00">22:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="23:00">23:00</div>\n' +
        '                                        <div class="tooltip__item" data-time="24:00">24:00</div>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>'
});

$(document).on('click', ".tooltip__item", function () {
    if ($(this).hasClass('disabled') || $(this).hasClass('active')) {
    } else {
        $('.tooltip__item.active').removeClass('active');
        $(this).addClass('active');
        $('.js-timepicker').val($(this)[0].innerText).popover('hide');
    }
});

$('.js-timepicker').on('inserted.bs.popover', function () {
    var selectData = $('.js-datapicker').val();
    selectData = selectData.split("/");
    var month = {
        '01': 'January',
        '02': 'February',
        '03': 'March',
        '04': 'April',
        '05': 'May',
        '06': 'June',
        '07': 'July',
        '08': 'August',
        '09': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December',
    };

    selectData = month[selectData[0]] + ' ' + parseInt(selectData[1]) + ', ' + selectData[2];

    $('.tooltip__time--header').text(selectData);
});

$('.message-popup__close').click(function () {
   $(this).closest('div.message-popup').removeClass('show');
});

$('.js-datapicker').click( function () {
    $('.js-timepicker').popover('hide');
});
