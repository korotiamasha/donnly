$(document).ready(function(){
    $('.navbar-toggler').click(function(){
        $(this).toggleClass('open');
    });
});

$(window).scroll(function () {
    var top = $(window).scrollTop();
    if (top > 0) {
        $(".header").addClass("fixed");
    } else {
        $(".header").removeClass("fixed");
    }
});
